#!/bin/bash

# Use Espeak-ng to create sound files
# This is done separately, you will
# need to have Espeak-ng installed on the
# machine where the sounds are created
if [ ! -d sounds ]; then
  mkdir sounds
fi


words=("Moja" "Mbili" "Tatu" "Nne" "Tano" "Sita" 
           "Saba" "Nane" "Tisa" "Kumi" "Sahihi" "Batili" 
           "Umejaribu" "Ndugu, matatu ngapi?" "Hesabu")
count=0
for word in ${!words[*]}
do
        ((count+=1))
        echo ${words[$word]}
        filename=sounds/${count}.wav
        if [ ! -f sounds/${count}.wav ]; then
          espeak-ng -v swahili "${words[$word]}" -w ${filename}
        fi
done
