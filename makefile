SRC = SwahiliNumbers.c
OBJ = ${SRC:.c=.o}

CFLAGS = `sdl2-config --libs --cflags` -O0 --std=c99 -D_REENTRANT -Wall
CC = gcc
INCLUDE = /usr/include/SDL2
LIBS = -lSDL2 -lSDL2_image -lSDL2_ttf -lm

SwahiliNumbers: ${OBJ}
	${CC} ${CFLAGS} ${INCLUDES} -o $@ $^ ${LIBS}

clean:
	rm -f *.o SwahiliNumbers

