%define build_timestamp %{lua: print(os.date("%Y%m%d"))}
%define version 0.10
%define name Nambari
%define reponame nambari

Summary: Game to learn numbers
Name: %{name}
Version: 0.10
Release: %{build_timestamp}
License: BSD 3-Clause License
Prefix: /usr/bin
URL: https://nairuby-games.gitlab.io/nambari/
Source0: https://gitlab.com/nairuby-games/nambari/-/archive/master/nambari-master.tar.gz
BuildRequires: gcc
BuildRequires: SDL2-devel 
BuildRequires: SDL2_image-devel 
BuildRequires: SDL2_ttf-devel
BuildRequires: espeak-ng-devel

Requires: SDL2
Requires: expeak-ng
Requires: SDL2_image
Requires: SDL2_ttf

%description
Numeracy game for children

%prep
%setup -q -n %{reponame}-master

%make_build

%install
rm -rf ${buildroot}
mkdir %{buildroot}%{prefix} -p
cp SwahiliNumbers %{buildroot}%{prefix}

%clean
rm -rf %{buildroot}

%files
%{prefix}/SwahiliNumbers

%changelog
* Tue Dec 29 2020 Benson Muite - 0.10
- Remove dependence on SDL2_mixer
* Sun Dec 27 2020 Benson Muite - 0.10
- Update requirements
* Sat Dec 26 2020 Benson Muite - 0.10
- Initial creation
