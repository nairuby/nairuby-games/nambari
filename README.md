# Nambari

Hii ni mchezo ya kusomesha watoto kuhesabu

Unaweza kupata programmu za Linux [hapa](https://download.opensuse.org/repositories/home:/bokbms:/Nambari/)

## Kutengeneza kwenye Fedora Linux

```sh
git clone https://gitlab.com/nairuby-games/nambari
cd nambari
sudo dnf -y groupinstall "Development Tools"
sudo dnf -y install gcc SDL2-devel SDL2_image-devel SDL2_ttf-devel 
bash createsounds.sh
make
./SwahiliNumbers
```

## Kutengeneza kwenye Ubuntu | Debian

```sh
git clone https://gitlab.com/nairuby-games/nambari
cd nambari
sudo apt install build-essential
sudo apt install gcc libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev
bash createsounds.sh
make
./SwahiliNumbers
```

## Fonti

Kutoka [https://github.com/tonsky/FiraCode](https://github.com/tonsky/FiraCode)  [OFL-1.1](https://opensource.org/licenses/OFL-1.1)


## Picha

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Matatu_Manyanga.jpg/244px-Matatu_Manyanga.jpg)

[https://commons.wikimedia.org/wiki/File:Matatu_Manyanga.jpg](https://commons.wikimedia.org/wiki/File:Matatu_Manyanga.jpg)  [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![](https://upload.wikimedia.org/wikipedia/commons/8/87/A_matatu.jpg)

[https://commons.wikimedia.org/wiki/File:A_matatu.jpg](https://commons.wikimedia.org/wiki/File:A_matatu.jpg) [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Matatu4.jpg/320px-Matatu4.jpg)

[https://commons.wikimedia.org/wiki/File:Matatu4.jpg](https://commons.wikimedia.org/wiki/File:Matatu4.jpg)  [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Matatus_Kampala.jpg/320px-Matatus_Kampala.jpg)

[https://commons.wikimedia.org/wiki/File:Matatus_Kampala.jpg](https://commons.wikimedia.org/wiki/File:Matatus_Kampala.jpg) [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/MATATU_MATWANA_CULTURE_KENYA_04.jpg/240px-MATATU_MATWANA_CULTURE_KENYA_04.jpg)

[https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg](https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg) [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Blue_Matatu_with_colourful_graphiti.jpg/320px-Blue_Matatu_with_colourful_graphiti.jpg)

[https://commons.wikimedia.org/wiki/File:Blue_Matatu_with_colourful_graphiti.jpg](https://commons.wikimedia.org/wiki/File:Blue_Matatu_with_colourful_graphiti.jpg) [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

## Jisomesha SDL2

- [https://www.geeksforgeeks.org/sdl-library-in-c-c-with-examples/](https://www.geeksforgeeks.org/sdl-library-in-c-c-with-examples/)

- [https://gigi.nullneuron.net/gigilabs/writing/sdl2-tutorials/](https://gigi.nullneuron.net/gigilabs/writing/sdl2-tutorials/)

- [https://wiki.libsdl.org/FrontPage](https://wiki.libsdl.org/FrontPage)

- [http://lazyfoo.net/SDL_tutorials](http://lazyfoo.net/SDL_tutorials)

- [https://vladar4.github.io/sdl2_nim/](https://vladar4.github.io/sdl2_nim/)

# Numbers

A game to teach children how to count

Linux executables available [here](https://download.opensuse.org/repositories/home:/bokbms:/Nambari/)

## To compile on Fedora Linux

```sh
git clone https://gitlab.com/nairuby-games/nambari
cd nambari
sudo dnf -y groupinstall "Development Tools"
sudo dnf -y install gcc SDL2-devel SDL2_image-devel SDL2_ttf-devel 
bash createsounds.sh
make
./SwahiliNumbers
```

## To compile on Ubuntu | Debain

```sh
git clone https://gitlab.com/nairuby-games/nambari
cd nambari
sudo apt install build-essential
sudo apt install gcc libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev
bash createsounds.sh
make
./SwahiliNumbers
```

## Fonts

From [https://github.com/tonsky/FiraCode](https://github.com/tonsky/FiraCode)  [OFL-1.1](https://opensource.org/licenses/OFL-1.1)

## Pictures

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Matatu_Manyanga.jpg/244px-Matatu_Manyanga.jpg)

[https://commons.wikimedia.org/wiki/File:Matatu_Manyanga.jpg](https://commons.wikimedia.org/wiki/File:Matatu_Manyanga.jpg)  [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![](https://upload.wikimedia.org/wikipedia/commons/8/87/A_matatu.jpg)

[https://commons.wikimedia.org/wiki/File:A_matatu.jpg](https://commons.wikimedia.org/wiki/File:A_matatu.jpg) [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Matatu4.jpg/320px-Matatu4.jpg)

[https://commons.wikimedia.org/wiki/File:Matatu4.jpg](https://commons.wikimedia.org/wiki/File:Matatu4.jpg)  [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Matatus_Kampala.jpg/320px-Matatus_Kampala.jpg)

[https://commons.wikimedia.org/wiki/File:Matatus_Kampala.jpg](https://commons.wikimedia.org/wiki/File:Matatus_Kampala.jpg) [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/MATATU_MATWANA_CULTURE_KENYA_04.jpg/240px-MATATU_MATWANA_CULTURE_KENYA_04.jpg)

[https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg](https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg) [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Blue_Matatu_with_colourful_graphiti.jpg/320px-Blue_Matatu_with_colourful_graphiti.jpg)

[https://commons.wikimedia.org/wiki/File:Blue_Matatu_with_colourful_graphiti.jpg](https://commons.wikimedia.org/wiki/File:Blue_Matatu_with_colourful_graphiti.jpg) [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

## Teach yourself SDL2

- [https://www.geeksforgeeks.org/sdl-library-in-c-c-with-examples/](https://www.geeksforgeeks.org/sdl-library-in-c-c-with-examples/)

- [https://gigi.nullneuron.net/gigilabs/writing/sdl2-tutorials/](https://gigi.nullneuron.net/gigilabs/writing/sdl2-tutorials/)

- [https://wiki.libsdl.org/FrontPage](https://wiki.libsdl.org/FrontPage)

- [http://lazyfoo.net/SDL_tutorials](http://lazyfoo.net/SDL_tutorials)

- [https://vladar4.github.io/sdl2_nim/](https://vladar4.github.io/sdl2_nim/)
